<!DOCTYPE html>
<html:html>
<head>
<meta charset="utf-8">
<title>Home</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/app/css/styles.css">
</head>
<body>
    <div id="wrapper">
        <h1>Hello world!</h1>
        <p>The time on the server is ${serverTime}.</p>

        <html:link href="http://www.ne.jp/asahi/hishidama/home/tech/struts/tag.html">Usage of Struts Tags</html:link>
    </div>
</body>
</html:html>
